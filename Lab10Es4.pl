%4.1
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

%4.2
seqR(0,[0]).
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

%4.3
seqR2(N,L2):- seqR(N,L), reverse(L, L2).