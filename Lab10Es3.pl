%3.1
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

%3.2
all_bigger([], []).
all_bigger([X|Xs],[Y|Ys]):- X > Y, all_bigger(Xs, Ys).

%3.3
search(X, [X|_]).
search(X, [_|Xs]):- search(X, Xs).

%3.4
sublist([],_).
sublist([X|Xs], [X|Ys]) :- sublist(Xs, Ys).
sublist([X|Xs], [_|Ys]) :- sublist([X|Xs], Ys).