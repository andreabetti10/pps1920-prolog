%2.1
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

%2.2
size2([],zero).
size2([_|T],s(N)) :- size2(T,N).

%2.3
sum([],0).
sum([X|Xs],M) :- sum(Xs,N), M is N+X.

%2.4
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :- C2 is C+1, S2 is S+X, average(Xs,C2,S2,A).

%2.5
max([X|Xs],Max) :- max([X|Xs],Max,X).
max([],Max,TempMax) :- Max is TempMax.
max([X|Xs],Max,TempMax) :- X > TempMax, max(Xs,Max,X).
max([X|Xs],Max,TempMax) :- X =< TempMax, max(Xs,Max,TempMax).