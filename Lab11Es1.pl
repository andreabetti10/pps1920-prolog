dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

dropLast(X, HT, L) :- reverse(HT, HT2), dropFirst(X, HT2, L2), reverse(L2, L).

dropAll(X,[],[]) :- !.
dropAll(X,[X|T],L) :- !, dropAll(X, T, L).
dropAll(X,[H|Xs],[H|L]) :- dropAll(X, Xs, L).