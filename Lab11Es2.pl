%2.1
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

%2.2
last([],X,[X]).
last([H|T],X,[H|L]):- !, last(T,X,L).

fromCircList([H1|T],LG) :- last([H1|T],H1,L), last(_, Last, [H1|T]),
													last(G,e(Last,H1),LG),
													fromList(L,LG),
													!.

%2.3
dropAll(X,[],[]) :- !.
dropAll(X,[X|T],L) :- !, dropAll(X, T, L).
dropAll(X,[H|Xs],[H|L]) :- dropAll(X, Xs, L).

dropNode(G,N,O):- dropAll(G,e(N,_),G2), dropAll(G2,e(_,N),O).

%2.4
reaching(G,N,L):- findall(Node,member(e(N,Node),G),L).

%2.5
anypath(G, N1, N2, [e(N1, N2)]):- member(e(N1, N2), G).
anypath(G, N1, N3, [e(N1, N2)|T]):- member(e(N1, N2), G), anypath(G, N2, N3, T).

%2.6
allreaching(G, N, L) :- findall(Node, anypath(G, N, Node, _), L).