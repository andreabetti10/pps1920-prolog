%1.1
search(X, [X|_]).
search(X, [_|Xs]) :- search(X, Xs).

%1.2
search2(X, [X,X|_]).
search2(X, [_|Xs]):- search2(X,Xs).

%1.3
search_two(X, [X,_,X|_]).
search_two(X, [_|Xs]):- search_two(X,Xs).

%1.4
search_anytwo(X, [X|Y]):- search(X,Y).
search_anytwo(X, [_|Xs]):- search_anytwo(X,Xs).